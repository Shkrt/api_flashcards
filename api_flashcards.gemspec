$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "api_flashcards/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "api_flashcards"
  s.version     = ApiFlashcards::VERSION
  s.authors     = ["Shkrt"]
  s.email       = ["test@te.st"]
  s.homepage    = "http://github.com/Shkrt/api_flashcards"
  s.summary     = "ApiFlashcards."
  s.description = "ApiFlashcards."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", "~> 4.2.1"
  s.add_dependency "rails-api"
  s.add_dependency "active_model_serializers"
  s.add_dependency "swagger-blocks"

  s.add_development_dependency "pg"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "rspec"
  s.add_development_dependency "bcrypt"
  s.add_development_dependency "capybara"
  s.add_development_dependency "factory_girl_rails"
  s.add_development_dependency "swagger_engine"
  s.add_development_dependency "sass"
  s.add_development_dependency "json_matchers"
  s.add_development_dependency "levenshtein"
end
