require "rubygems"
require "swagger_engine"
require "levenshtein"
module ApiFlashcards
  class Engine < ::Rails::Engine
    isolate_namespace ApiFlashcards
  end
end
