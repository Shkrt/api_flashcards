require "rails-api/action_controller/api"
require "active_model_serializers"

module ApiFlashcards
  class ApplicationController < ActionController::API
    attr_reader :current_user
    include ActionController::Serialization
    include ActionController::HttpAuthentication::Basic::ControllerMethods
    before_action :authenticate_basic

    private

    def authenticate_basic
      authenticate_or_request_with_http_basic do |email, passwd|
        auth = User.authenticate(email, passwd)
        if auth
          @current_user = User.where(email: email).first
        else
          render json: { message: "Authentication failed" }, status: 401
        end
      end
    end
  end
end
