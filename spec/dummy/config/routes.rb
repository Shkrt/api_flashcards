Rails.application.routes.draw do
  mount ApiFlashcards::Engine => "/api_flashcards"
  mount SwaggerEngine::Engine, at: "/api-docs"
end
