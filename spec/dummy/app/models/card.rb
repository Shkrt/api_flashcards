class Card < ActiveRecord::Base
  belongs_to :user
  belongs_to :block
  validates :original_text, :translated_text, :review_date, presence: true
  validates :user_id, presence: true
  validates :block_id, presence: true
  validates :attempt, :interval, presence: true

  scope :pending, -> { where("review_date <= ?", Time.now).order("RANDOM()") }
  scope :repeating, -> { where("quality < ?", 4).order("RANDOM()") }

  def check_translation(user_translation)
    distance = Levenshtein.distance(full_downcase(translated_text),
                                    full_downcase(user_translation))
    parameters = set_parameters(distance, interval, review_date, attempt)
    { state: parameters[:state], message: set_message(distance, user_translation, translated_text) }
  end

  protected

  def full_downcase(str)
    str.mb_chars.downcase.to_s.squeeze(' ').lstrip
  end

  def set_message(dist, user_t, trans_t)
    if dist > 1
      { type: "alert", text: I18n.t("incorrect_translation_alert") }
    elsif dist == 0
      { type: "notice", text: I18n.t("correct_translation_notice") }
    else
      { type: "alert", text: (I18n.t "translation_from_misprint_alert",
                  translated_text: trans_t, user_translation: user_t) }
    end
  end

  def set_parameters(dist, intl, rev, attm)
    if dist > 1
      { review_date: rev, attempt: [attm + 1, 5].min, state: false }
    else
      { review_date: intl.to_i.days.from_now, attempt: 1, state: true }
    end
  end
end
