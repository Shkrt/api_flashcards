class User < ActiveRecord::Base
  has_secure_password

  def self.authenticate(email, password)
    user = User.where(email: email).first
    user.authenticate(password) if user
  end

  has_many :cards, dependent: :destroy
  has_many :blocks, dependent: :destroy
  belongs_to :current_block, class_name: "Block"

  validates :password, presence: true, length: { minimum: 3 }
  validates :email, uniqueness: true, presence: true,
                    format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/ }

  def get_card
    range = current_block ? current_block.cards : cards
    range.pending.first || range.repeating.first
  end

  def set_current_block(block)
    update_attribute(:current_block_id, block.id)
  end

  def reset_current_block
    update_attribute(:current_block_id, nil)
  end
end
