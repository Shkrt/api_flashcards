ApiFlashcards::Engine.routes.draw do

  scope module: "api" do
    namespace "v1" do
      get "/" => "docs#index"
      get "test/hello" => "test#hello"
      get "cards" => "cards#index"
      post "cards" => "cards#create"
      put "review_card" => "trainer#review_card"
      get "trainer" => "trainer#index"
    end
  end
end
